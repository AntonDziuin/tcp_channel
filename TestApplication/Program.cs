﻿using System;
using System.Text;
using System.Threading;
using TCP_channel;
namespace TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int port = 5678;

            Client client = new Client();
            client.enableLogException = false;
            client.Start("localhost", port);
            client.SendMessage(Encoding.UTF8.GetBytes("Hello from client"));
            Thread.Sleep(500);
            client.SendMessage(Encoding.UTF8.GetBytes("Hello from client"));
            Thread.Sleep(500); client.SendMessage(Encoding.UTF8.GetBytes("Hello from client"));
            Thread.Sleep(500); client.SendMessage(Encoding.UTF8.GetBytes("Hello from client"));
            Thread.Sleep(500);
            Server server = new Server();
            Server.enableLogException = true;
            server.Start(port);

         

            server.dataReceived += Server_dataReceived;
            client.dataReceived += Client_dataReceived;

            while (true)
            {
                client.SendMessage(Encoding.UTF8.GetBytes("Hello from client"));
                Thread.Sleep(500);
                server.SendMessage(Encoding.UTF8.GetBytes("Hello from server"));
                Thread.Sleep(500);
            }
        }

        private static void Client_dataReceived(byte[] data)
        {
            string serverMessage = Encoding.ASCII.GetString(data);
            Console.WriteLine(serverMessage);
        }

        private static void Server_dataReceived(byte[] data)
        {
            string clientMessage = Encoding.ASCII.GetString(data);
            Console.WriteLine(clientMessage);
        }
    }
}
