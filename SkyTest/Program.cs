﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using TCP_channel;
namespace SkyTest
{
    class Program
    {
        static Server server = new Server();
        
        static void Main(string[] args)
        {
            int port = 5566;
            server.Start(port);
            Server.enableLogException = true;
            server.dataReceived += Server_dataReceived;
            char consoleChar;
            do
            {
                consoleChar = Console.ReadKey().KeyChar;
            } while (consoleChar != 'e' || consoleChar != 'E');
        }
        static List<byte> qq = new List<byte>();
        private static void Server_dataReceived(byte[] data)
        {
            foreach(byte b in data)
            {
                if (b == 10)
                {
                    Console.WriteLine("Server_dataReceived: " + BitConverter.ToString(qq.ToArray()) + " : " + Encoding.ASCII.GetString(qq.ToArray()));
                    qq.Clear();
                    break;
                }
                else
                {
                    qq.Add(b);
                }
            }
          //  Thread.Sleep(5);
            
          //  server.SendMessage(data);
        }
    }
}
