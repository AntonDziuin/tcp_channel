﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace TCP_channel
{
    public class Server
    {
        public static bool enableLogException = true;

        public TcpListener tcpListener;

        private Thread tcpListenerThread;

        private volatile TcpClient connectedTcpClient;

        public delegate void receivedDatafromClient(byte[] data);
        public event receivedDatafromClient dataReceived;

        private int port =0;

        public void Start(int port=0)
        {
            if (port != 0)
            {
                this.port = port;
                StartListener();
            }
            else
            {
                if (this.port != 0)
                {
                    StartListener();
                }
                else
                {
                    Console.WriteLine("port is invalid");
                }
            }
        }
        private void StartListener()
        {
            tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
            tcpListenerThread.IsBackground = true;
            tcpListenerThread.Start();
        }
        private void ListenForIncommingRequests()
        {
            try
            {			
                tcpListener = new TcpListener(IPAddress.Any, port);
                tcpListener.Start();
                Console.WriteLine("Server is listening");
                Byte[] bytes = new Byte[1024];
                while (true)
                {
                    using (connectedTcpClient = tcpListener.AcceptTcpClient())
                    {
                        Console.WriteLine("Client connected " + connectedTcpClient.Client.RemoteEndPoint);
                        using (NetworkStream stream = connectedTcpClient.GetStream())
                        {
                            int length;					
                            while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                            {
                                var incommingData = new byte[length];
                                Array.Copy(bytes, 0, incommingData, 0, length);
                                if (dataReceived!=null)
                                dataReceived(incommingData);
                            }
                        }
                    }
                    Console.WriteLine("Client disconnected " + connectedTcpClient.Client.RemoteEndPoint);
                }
            }
            catch (SocketException socketException)
            {
                LogToConsole("SocketException " + socketException.ToString());
                Restart();
            }
            catch (IOException exeption)
            {
                LogToConsole("IOException " + exeption.Message);
                connectedTcpClient = null;
                Restart();
            }
            catch (InvalidOperationException exception)
            {
                Restart();
            }
        }
        public void SendMessage(byte[] data)
        {
            if (connectedTcpClient == null)
            {
                return;
            }

            try
            {
                NetworkStream stream = connectedTcpClient.GetStream();
                if (stream.CanWrite)
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            catch (SocketException socketException)
            {
                LogToConsole("Socket exception: " + socketException);
                Restart();
            }
            catch (ObjectDisposedException odispException)
            {
                LogToConsole("ObjectDisposedException: " + odispException);
                Restart();
            }
        }

        static void LogToConsole(string msg)
        {
            if (enableLogException)
            {
                Console.WriteLine(msg);
            }
        }
        void Restart()
        {
            tcpListener.Stop();
            connectedTcpClient = null;
            Start();
        }
    }
}
