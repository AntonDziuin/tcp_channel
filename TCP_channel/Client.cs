﻿using System;
using System.Net.Sockets;
using System.Threading;

namespace TCP_channel
{
    public class Client
    {
        public bool enableLogException = true;

        public volatile TcpClient socketConnection;
        private Thread clientReceiveThread;

        public delegate void receivedDatafromClient(byte[] data);
        public event receivedDatafromClient dataReceived;

        private string address;
        private int port;
        public void Start(string address = null, int port=0)
        {
            if (address != null)
            {
                this.address = address;
                this.port = port;
            }
            try
            {
                socketConnection.Close();
                socketConnection = null;
            }
            catch (NullReferenceException nre)
            {
                Console.WriteLine("nre " + nre.Message);
            }
            ConnectToTcpServer();
        }

        private void ConnectToTcpServer()
        {
            try
            {
                clientReceiveThread = new Thread(new ThreadStart(ListenForData));
                clientReceiveThread.IsBackground = true;
                clientReceiveThread.Start();
            }
            catch (Exception e)
            {
                LogToConsole("On client connect exception " + e);
            }
        }

        private void ListenForData()
        {
            try
            {
                socketConnection = new TcpClient(address, port);
                Byte[] bytes = new Byte[1024];
                while (true)
                {
                    using (NetworkStream stream = socketConnection.GetStream())
                    {
                        int length;
                        while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            var incommingData = new byte[length];
                            Array.Copy(bytes, 0, incommingData, 0, length);
                            if (dataReceived!=null)
                            dataReceived(incommingData);

                        }
                    }
                }
            }
            catch (SocketException socketException)
            {
                LogToConsole("Socket exception: " + socketException);
                Start();
            }
            catch (System.IO.IOException ioException)
            {
                LogToConsole("ioException: " + ioException.Message);
                Start();
            }
        }

        int iteraor = 0;
        public void SendMessage(byte[] data)
        {
            if (socketConnection == null)
            {
                return;
            }
            try
            {
                NetworkStream stream = socketConnection.GetStream();
                if (stream.CanWrite)
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            catch (SocketException socketException)
            {
                LogToConsole("Socket exception: " + socketException);
                Start();
            }
            catch (System.InvalidOperationException invalidOperationException)
            {

                LogToConsole("invalidOperationException: " + invalidOperationException.Message);

                Start();
            }
        }

        void LogToConsole(string msg)
        {
            if (enableLogException)
            {
                Console.WriteLine(msg);
            }
        }
    }
}
