[tcp_channel(server&client).zip](https://drive.google.com/open?id=1sFVznsMO1_HhGqNcnikU156gwRq_PpNP)

Usage:
```csharp
Server server = new Server();

server.Start(port);

Client client = new Client();

client.Start("localhost", port);

server.dataReceived += Server_dataReceived;

client.dataReceived += Client_dataReceived;

client.SendMessage(Encoding.UTF8.GetBytes("Hello from client"));
                
server.SendMessage(Encoding.UTF8.GetBytes("Hello from server"));
```


